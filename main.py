from estimate_sound_direction_3mics import *
from rep_visuelle_3micros import *

# Gives a visual representation of the microphones and the bird in comparison with the microphones

# To complete (the files must be mono audio files):
audio_file1 = 'test/shifted_with_background_still/sound+noise1.mp3'  # path
audio_file2 = 'test/shifted_with_background_still/sound+noise2.mp3'  # path
audio_file3 = 'test/shifted_with_background_still/sound+noise3.mp3'  # path
distance_12 = 0.75  # distance between micro n°1 and 2
distance_23 = 0.5  # distance between micro n°1 and 2

if __name__ == '__main__':
    print("Calculating the bird's position")
    angle1, angle2 = estimate_sound_direction(
        audio_file1, audio_file2, audio_file3, distance_12, distance_23)
    print(angle1, angle2)
    plot_microphones_and_line(distance_12, distance_23, angle1, angle2)
