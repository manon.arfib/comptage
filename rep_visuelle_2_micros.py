import matplotlib.pyplot as plt
import numpy as np


def plot_microphones_and_line(distance, alpha):
    """Plots the 2 microphones and a line in the direction of the sound

    Args:
        distance (float): distance between the 2 microphones
        alpha (float): angle between the line of the microphones and the direction of the sound
    """

    alpha_rad = (alpha*np.pi)/180

    # Microphone positions
    microphone_positions = [-distance/2, distance/2]

    # Calculate line parameters
    x_line = np.linspace(-distance/2, distance/2, 1000)

    y_line = ((distance/2)/np.tan(alpha_rad))*(x_line-np.mean(x_line))

    # Filter out values based on angle range
    if 0 < alpha <= 90:
        x_filtered = x_line[x_line >= 0]
        y_filtered = y_line[y_line >= 0]
    elif -90 <= alpha < 0:
        x_filtered = x_line[x_line <= 0]
        y_filtered = y_line[y_line >= 0]
    elif 90 <= alpha <= 180:
        x_filtered = x_line[x_line >= 0]
        y_filtered = y_line[y_line <= 0]
    else:
        x_filtered = x_line[y_line <= 0]
        y_filtered = y_line[y_line <= 0]

    # Plotting
    fig, ax = plt.subplots()
    ax.plot(microphone_positions, [0, 0], 'bo', label='Microphones')
    if alpha == 0:
        plt.axvline(x=0, color='r', linestyle='-', label='Line')
    else:
        ax.plot(x_filtered, y_filtered, 'r-', label='Line')
    ax.set_xlabel('Position')
    ax.set_ylabel('Y-axis')
    ax.set_title('Microphones and Line')
    ax.legend()
    plt.grid()
    plt.show()


# Example usage
if __name__ == '__main__':
    distance_between_microphones = 5
    angle_alpha = -178  # in degrees
    plot_microphones_and_line(distance_between_microphones, angle_alpha)
