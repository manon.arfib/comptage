import matplotlib.pyplot as plt
import numpy as np
from pydub import AudioSegment
import audio2numpy as a2n

AudioSegment.converter = r"C:\ffmpeg\bin\ffmpeg.exe"


def estimate_sampling_frequency(audio_file):
    """
    Args:
        audio_file (path or string): relative path to the audio file

    Returns:
        int : sampling rate
    """
    s_r = a2n.audio_from_file(audio_file)[1]
    return s_r


def calculate_tdoa(audio1, audio2, fs):
    """Calculates the time difference of arrival between two audios recorded by different microphones

    Args:
        audio1 (array)
        audio2 (array)
        fs (int): sampling frequency (usually 44100Hz)

    Returns:
        float: tdoa between the 2 signals
    """

    # Calculate the cross correlation
    cross_correlation = np.correlate(audio1, audio2, mode='full')

    # Find the index corresponding to the max of correlation
    max_index = np.argmax(cross_correlation)-len(audio2)+1
    print('max_index=', max_index)

    # Calculate the Time Difference Of Arrival (TDOA) in seconds
    tdoa = max_index / fs

    return tdoa


def plot_cross_correlation(signal1, signal2):
    """Plots the 2 signals as a function of time and the cross correlation between them

    Args:
        signal1 (array)
        signal2 (array)
    """

    # Calculate the cross-correlation
    cross_correlation = np.correlate(signal1, signal2, mode='full')

    # Generate the time lag axis
    time_lag = np.arange(-len(signal2) + 1, len(signal1))

    # Find the index of the maximum correlation
    max_index = np.argmax(cross_correlation)
    adjusted_max_index = max_index - len(signal2) + 1

    # Plot the signals
    plt.subplot(3, 1, 1)
    plt.plot(signal1)
    plt.ylabel('Signal 1')

    plt.subplot(3, 1, 2)
    plt.plot(signal2)
    plt.ylabel('Signal 2')

    # Plot the cross-correlation
    plt.subplot(3, 1, 3)
    plt.plot(time_lag, cross_correlation)
    plt.xlabel('Time Lag')
    plt.ylabel('Cross-correlation')
    plt.axvline(x=adjusted_max_index, color='r',
                linestyle='--', label='Max Correlation')
    plt.legend()

    plt.tight_layout()
    plt.grid(True)
    plt.show()


def estimate_sound_direction(audio_file1, audio_file2, audio_file3, distance_12, distance_23, speed_of_sound=340):
    """Calculates the angle between the microphone and the source

    Args:
        audio_file1 (path or string)
        audio_file2 (path or string)
        audio_file3 (path or string)
        distance_12 (float): distance (in m) between the microphones 1 and 2
        distance_23 (float): distance (in m) between the microphones 2 and 3
        speed_of_sound (int, optional): Defaults to 340.

    Returns:
        (float,float) : the 2 angles in degrees
    """

    fs = estimate_sampling_frequency(audio_file1)

    # Load the recordings
    audio1 = a2n.audio_from_file(audio_file1)[0]
    audio2 = a2n.audio_from_file(audio_file2)[0]
    audio3 = a2n.audio_from_file(audio_file3)[0]

    # Normalize
    audio1 /= np.nanmax(np.abs(audio1))
    audio2 /= np.nanmax(np.abs(audio2))
    audio3 /= np.nanmax(np.abs(audio3))

    # Calculate the TDOAs
    tdoa_12 = calculate_tdoa(audio1, audio2, fs)
    tdoa_23 = calculate_tdoa(audio2, audio3, fs)
    print('tdoa :', (tdoa_12, tdoa_23))

    # Estimate the emission direction of the sound
    theta_12 = np.arcsin(tdoa_12 * speed_of_sound /
                         distance_12) * (180.0 / np.pi)
    theta_23 = np.arcsin(tdoa_23 * speed_of_sound /
                         distance_23) * (180.0 / np.pi)

    return (theta_12, theta_23)


if __name__ == '__main__':
    plot_cross_correlation(a2n.audio_from_file('test\shifted_with_background_still\sound+noise1.mp3')[0],
                           a2n.audio_from_file('test\shifted_with_background_still\sound+noise2.mp3')[0])
