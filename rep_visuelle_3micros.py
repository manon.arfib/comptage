import matplotlib.pyplot as plt
import numpy as np


def plot_microphones_and_line(distance1, distance2, alpha1, alpha2):
    """Plots the 3 microphones, 2 lines in the direction of the sound, and the location of the source

    Args:
        distance1 (float): distance (in m) between the microphones 1 and 2
        distance2 (float): distance (in m) between the microphones 2 and 3
        alpha1 (float): angle between the line of the microphones 1 and 2 and the direction of the sound
        alpha2 (float): angle between the line of the microphones 2 and 3 and the direction of the sound

    Returns:
        (float,float): coorinates of the source relatively of the microphones
    """

    alpha1_rad = (alpha1*np.pi)/180
    alpha2_rad = (alpha2*np.pi)/180

    # Microphone positions
    microphone_positions = [-distance1, 0, distance2]

    # slope1 = (distance1/2)/np.tan(alpha1_rad)
    # slope2 = (distance2/2)/np.tan(alpha2_rad)
    slope1 = 1/np.tan(alpha1_rad)
    slope2 = 1/np.tan(alpha2_rad)
    intercept1 = (distance1/2) * slope1
    intercept2 = -(distance2/2)*slope2

    intersection_x = (intercept2-intercept1)/(slope1-slope2)
    intersection_y = slope1*intersection_x + intercept1

    if intersection_x <= -distance1:
        x_line1 = np.linspace(intersection_x, -distance1/2, 1000)
        x_line2 = np.linspace(intersection_x, distance2/2, 1000)

    elif intersection_x > -distance2:
        x_line1 = np.linspace(-distance1/2, intersection_x, 1000)
        x_line2 = np.linspace(distance2/2, intersection_x, 1000)

    else:
        x_line1 = np.linspace(-distance1/2, intersection_x, 1000)
        x_line2 = np.linspace(intersection_x, distance2/2, 1000)

    y_line1 = slope1*x_line1+intercept1
    y_line2 = slope2*x_line2+intercept2

    # Plotting
    fig, ax = plt.subplots()
    ax.plot(microphone_positions, [0, 0, 0], 'bo', label='Microphones')
    if alpha1 == 0:
        plt.axvline(x=-distance1/2, color='r',
                    linestyle='-', label='Direction 1')
    if alpha1 != 0:
        ax.plot(x_line1, y_line1, 'r-', label='Direction 1')

    if alpha2 == 0:
        plt.axvline(x=distance2/2, color='r',
                    linestyle='-', label='Direction 2')
    if alpha2 != 0:
        ax.plot(x_line2, y_line2, 'r-', label='Direction 2')
    ax.plot(intersection_x, intersection_y, 'go',
            markersize=10, label="Localisation de l'oiseau")
    # ax.set_xlabel('Position')
    # ax.set_ylabel('Y-axis')
    ax.set_title("Position de l'oiseau relativement aux micros")
    ax.legend()
    plt.grid()
    plt.draw()
    plt.waitforbuttonpress(0)
    plt.close()

    return ((intersection_x, intersection_y))
