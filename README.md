# Localisation de source sonore


## Description
Dans le cadre du comptage d'oiseaux dans un enregistrement audio, il est nécessaire de les localiser. Une étude de la position en fonction du temps pourra alors permettre de déterminer s'il s'agit d'un seul ou plusieurs individus. Par manque de temps, ce git ne permet pas de mettre en pratique cette deuxième étape, mais permet la localisation de sources sonores.

## Installation
Les modules nécessaires sot listés dans le fichier 'requirements.txt'. 
Attention, ce travail utilise beaucoup le module pydub. Ce module requiert une installation spécifique, dont vous pourrez par exemple retrouver la procédure sous Windows ici : https://www.youtube.com/watch?v=d2Y0lGrRoMI

## Usage
Lancer le fichier main.py en l'état vous permettra de voir un exemple de localisation de source. Pour l'appliquer à vos fichiers vous n'avez qu'à mettre vos 3 fichiers correspondant aux audios enregistrés par les 3 micros et modifier les paramètres de la fonction main.
Le fichier test/create_new_audio_with_back_still.py vous permettra de simuler un décalage temporel pour 3 audios avec le même fond. 

## Roadmap
Un développement de ce git dans le futur pourrait être l'ajout de la mise en place effective du comptage des individus dans un enregistrement, à partir de la localisation déjà réalisée.

## Acknowledgment
Un grand merci à Fred Ngole-Mboula pour son aide et ses conseils précieux.