import numpy as np
from pydub import AudioSegment
import audio2numpy as a2n

AudioSegment.converter = r"C:\ffmpeg\bin\ffmpeg.exe"


def is_stereo_file(filename):
    """tests if an audio file is a stereo or mono audio

    Args:
        filename (path or string): path to the file to test

    Returns:
        Boolean: True if stereo, False if mono
    """
    try:
        audio = AudioSegment.from_file(filename)
        num_channels = audio.channels
        if num_channels == 2:
            return True  # Stereo
        else:
            return False  # Mono
    except Exception as e:
        print(f"Error: {e}")
        return None  # Error occurred


def stereo_to_mono(input_file, output_file):
    """Converts a stereo audio file to a mono audio file and saves it

    Args:
        input_file (path or string): path to the file
        output_file (path or string): path (including name) of the file that is going to be saved
    """
    audio = AudioSegment.from_file(input_file)
    mono_audio = audio.set_channels(1)
    mono_audio.export(output_file, format="mp3")


def write(f, sr, x, normalized=False):
    """Converts a numpy array to a mp3 file and saves it

    Args:
        f (string or path): path that the file is going to be saved to
        sr (float): sampling rate (frequency), usually 44100
        x (array): array to be converted
        normalized (bool, optional): if the array is normalized or not. Defaults to False.
    """
    channels = 2 if (x.ndim == 2 and x.shape[1] == 2) else 1
    if normalized:  # normalized array - each item should be a float in [-1, 1)
        y = np.int16(x * 2 ** 15)
    else:
        y = np.int16(x)
    song = AudioSegment(y.tobytes(), frame_rate=sr,
                        sample_width=2, channels=channels)
    song.export(f, format="mp3", bitrate="320k")


def create_3_shifted_audios(noise_file, sound_file, snr, final_path):
    """Creates and saves 3 audio files with the noise starting at the same time but the sound slightly late for each of them

    Args:
        noise_file (path or string)
        sound_file (path or string)
        snr (float): sound to noise ratio wanted
        final_path (path or string): path (including name and extension) that the files are going to be saved to
    """

    sr = a2n.audio_from_file(noise_file)[1]  # sampling rate

    if is_stereo_file(noise_file):
        stereo_to_mono(noise_file, noise_file[:-4]+'_mono.mp3')
        noise = a2n.audio_from_file(noise_file[:-4]+'_mono.mp3')[0]

    else:
        noise = a2n.audio_from_file(noise_file)

    if is_stereo_file(sound_file):
        stereo_to_mono(sound_file, sound_file[:-4]+'_mono.mp3')
        # only keep the 10 first seconds to reduce the processing time
        sound = a2n.audio_from_file(sound_file[:-4]+'_mono.mp3')[0][:421000]

    else:
        # only keep the 10 first seconds to reduce the processing time
        sound = a2n.audio_from_file(sound_file)[:421000]

    sound1 = np.pad(sound, (120, 0), mode='constant')  # to modify as you wish
    sound2 = np.pad(sound, (47, 73), mode='constant')  # idem
    sound3 = np.pad(sound, (0, 120), mode='constant')  # idem

    # Normalize
    noise /= np.nanmax(np.abs(noise))
    noise = noise[:len(sound1)]
    sound1 /= np.nanmax(np.abs(sound1))
    sound2 /= np.nanmax(np.abs(sound2))
    sound3 /= np.nanmax(np.abs(sound3))

    n = np.mean(noise)
    s1 = np.mean(sound1)
    s2 = np.mean(sound2)
    s3 = np.mean(sound3)

    total1 = sound1*(snr*n/(snr*n+s1))+noise*(1-(snr*n/(snr*n+s1)))
    total1 /= np.nanmax(np.abs(total1))
    total2 = sound2*(snr*n/(snr*n+s2))+noise*(1-(snr*n/(snr*n+s2)))
    total2 /= np.nanmax(np.abs(total2))
    total3 = sound3*(snr*n/(snr*n+s3))+noise*(1-(snr*n/(snr*n+s3)))
    total3 /= np.nanmax(np.abs(total3))

    write(final_path[:-4]+'1.mp3', sr, total1, normalized=True)
    write(final_path[:-4]+'2.mp3', sr, total2, normalized=True)
    write(final_path[:-4]+'3.mp3', sr, total3, normalized=True)


if __name__ == '__main__':
    create_3_shifted_audios('test/bruit.mp3', 'test/oiseau.mp3',
                            1, 'test/shifted_with_background_still/sound+noise.mp3')
