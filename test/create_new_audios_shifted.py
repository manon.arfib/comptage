import matplotlib.pyplot as plt
import numpy as np
from pydub import AudioSegment
import audio2numpy as a2n

AudioSegment.converter = r"C:\ffmpeg\bin\ffmpeg.exe"


def write(f, sr, x, normalized=False):
    """Converts a numpy array to a mp3 file and saves it

    Args:
        f (string or path): path that the file is going to be saved to
        sr (float): sampling rate (frequency), usually 44100
        x (array): array to be converted
        normalized (bool, optional): if the array is normalized or not. Defaults to False.
    """
    channels = 2 if (x.ndim == 2 and x.shape[1] == 2) else 1
    if normalized:  # normalized array - each item should be a float in [-1, 1)
        y = np.int16(x * 2 ** 15)
    else:
        y = np.int16(x)
    song = AudioSegment(y.tobytes(), frame_rate=sr,
                        sample_width=2, channels=channels)
    song.export(f, format="mp3", bitrate="320k")


def is_stereo_file(filename):
    """tests if an audio file is a stereo or mono audio

    Args:
        filename (path or string): path to the file to test

    Returns:
        Boolean: True if stereo, False if mono
    """
    try:
        audio = AudioSegment.from_file(filename)
        num_channels = audio.channels
        if num_channels == 2:
            return True  # Stereo
        else:
            return False  # Mono
    except Exception as e:
        print(f"Error: {e}")
        return None  # Error occurred


def stereo_to_mono(input_file, output_file):
    """Converts a stereo audio file to a mono audio file and saves it

    Args:
        input_file (path or string): path to the file
        output_file (path or string): path (including name) of the file that is going to be saved
    """
    audio = AudioSegment.from_file(input_file)
    mono_audio = audio.set_channels(1)
    mono_audio.export(output_file, format="mp3")


if is_stereo_file('test/test.mp3'):
    stereo_to_mono('test/test.mp3', 'test/test_mono.mp3')

sound_array, sr = a2n.audio_from_file('test/test_mono.mp3')

audio1 = np.pad(sound_array,
                (0, 120), mode='constant')
audio2 = np.pad(sound_array,
                (73, 47), mode='constant')
audio3 = np.pad(sound_array,
                (120, 0), mode='constant')

write('test/audio1.mp3', sr, audio1, normalized=True)
write('test/audio2.mp3', sr, audio2, normalized=True)
write('test/audio3.mp3', sr, audio3, normalized=True)


# plt.subplot(4, 1, 1)
# plt.plot(a2n.audio_from_file('test/test_mono.mp3')[0])
# plt.subplot(4, 1, 2)
# plt.plot(a2n.audio_from_file('test/audio1.mp3')[0])
# plt.subplot(4, 1, 3)
# plt.plot(a2n.audio_from_file('test/audio2.mp3')[0])
# plt.subplot(4, 1, 4)
# plt.plot(a2n.audio_from_file('test/audio3.mp3')[0])
# plt.show()


def plot_cross_correlation(signal1, signal2):
    # Calculate the cross-correlation
    cross_correlation = np.correlate(signal1, signal2, mode='full')

    # Generate the time lag axis
    time_lag = np.arange(-len(signal2) + 1, len(signal1))

    # Find the index of the maximum correlation
    max_index = np.argmax(cross_correlation)
    adjusted_max_index = max_index - len(signal2) + 1

    # Plot the signals
    plt.subplot(3, 1, 1)
    plt.plot(signal1)
    plt.ylabel('Signal 1')

    plt.subplot(3, 1, 2)
    plt.plot(signal2)
    plt.ylabel('Signal 2')

    # Plot the cross-correlation
    plt.subplot(3, 1, 3)
    plt.plot(time_lag, cross_correlation)
    plt.xlabel('Time Lag')
    plt.ylabel('Cross-correlation')
    plt.axvline(x=adjusted_max_index, color='r',
                linestyle='--', label='Max Correlation')
    plt.legend()

    plt.tight_layout()
    plt.grid(True)
    plt.show()


# plot_cross_correlation(a2n.audio_from_file(
#     'test/audio1.mp3')[0], a2n.audio_from_file('test/audio2.mp3')[0])
